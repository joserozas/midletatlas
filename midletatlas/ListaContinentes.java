package midletatlas;

import javax.microedition.lcdui.*;

/**
 * Clase basada en una lista implícita (hereda de List) que contiene la lista de
 * continentes. Al elegir alguno de esos
 * continentes se saltará a una nueva pantalla con información sobre ese continente
 *
 * @author Jose Manuel Rozas Vilar
 */
public class ListaContinentes extends List implements CommandListener {

    private final MidletAtlas miMidlet;
    private final Command cmdInstrucciones, cmdSalir;
    private final String[] opcionesContinentes;
    private final InfoAtlas infoAtlas;

    /**
     * Constructor
     *
     * @param titulo
     * @param tipoLista
     * @param miMidlet
     */
    public ListaContinentes(String titulo, int tipoLista, MidletAtlas miMidlet) {
        super(titulo, tipoLista);

        this.miMidlet = miMidlet;

        infoAtlas = new InfoAtlas();
        // Insertar elementos en la lista:
        this.opcionesContinentes = infoAtlas.nombreContinentes;
        for (int i = 0; i < opcionesContinentes.length; i++) {
            this.insert(i, opcionesContinentes[i], null);
        }

        cmdInstrucciones = new Command("Instrucciones", Command.OK, 1);
        cmdSalir = new Command("Salir", Command.EXIT, 1);
        this.addCommand(cmdInstrucciones);
        this.addCommand(cmdSalir);

        this.setCommandListener(this);
    }

    /**
     *
     * @param c
     * @param d
     */
    public void commandAction(Command c, Displayable d) {
        if (c == cmdSalir) {
            miMidlet.mostrarCreditos();
        } else if (c == cmdInstrucciones) {
            miMidlet.mostrarFormInstrucciones();
        } else if (c == List.SELECT_COMMAND) {
            int indiceContinente = this.getSelectedIndex();
            // 0:  africa.
            // 1:  america
            // 2:  asia
            // 3:  europa
            // 4:  oceania
            String nombreContinente = infoAtlas.nombreContinentes[indiceContinente];
            miMidlet.mostrarContinente(nombreContinente, indiceContinente);
        }
    }
}
