package midletatlas;

import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

/**
 * Clase principal que hereda de midlet y se encarga de gestionar las distintas pantallas
 *
 * @author Jose Manuel Rozas Vilar
 */
public class MidletAtlas extends MIDlet {

    // El objeto que representa a la pantalla del dispositivo:
    private final Display pantalla;

    // Canvas con la imagen y el título de presentación:
    private final CanvasPresentacion canvasPresentacion;

    // Pantalla con las instrucciones:
    private final FormInstrucciones formInstrucciones;

    // Pantalla que muestra la lista de los 5 continentes:
    private final ListaContinentes listaContinentes;

    // Pantalla que mostrará el continente seleccionado,
    // un breve texto descriptivo y 3 países para seleccionar:
    private FormContinente continente;

    // Pantalla que mostrará 3 países del continente seleccionado anteriormente:
    private FormPaises paises;

    // Pantalla de créditos, que se mostrará antes de salir definitivamente,
    // mostrando nombre de la app, nombre del autor,
    // su foto y la fecha de creación:
    private final FormCreditos formCreditos;

    /**
     * Constructor. Se inicializan los formularios
     * que no necesitan parámetros adicionales de parte del usuario.
     */
    public MidletAtlas() {
        // Display:
        pantalla = Display.getDisplay(this);

        // Canvas con la imagen y el título de presentación:
        canvasPresentacion = new CanvasPresentacion(this);

        // Lista implícita con los continentes:
        listaContinentes = new ListaContinentes("Lista continentes", List.IMPLICIT, this);

        // Formulario mostrando un texto con las instrucciones:
        formInstrucciones = new FormInstrucciones("Instrucciones", this);

        // Formulario final antes de salir:
        formCreditos = new FormCreditos("Créditos", this);
    }

    public void startApp() {
        mostrarCanvasPresentacion();
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
    }

    public void salirMidlet() {
        destroyApp(true);
        notifyDestroyed();
    }

    /**
     * Método que es llamado sólo una vez al inicio, desde startApp(),
     * que muestra un canvas con la presentación.
     */
    protected void mostrarCanvasPresentacion() {
        pantalla.setCurrent(canvasPresentacion);
    }

    /**
     * Método para mostrar la pantalla con el formulario de las instrucciones.
     */
    protected void mostrarFormInstrucciones() {
        pantalla.setCurrent(formInstrucciones);
    }

    /**
     * Método para mostrar el formulario inicial de selección de los 5 continentes.
     */
    protected void mostrarListaContinentes() {
        pantalla.setCurrent(listaContinentes);
    }

    /**
     * Método que muestra el formulario con el continente elegido.
     *
     * @param nombreContinente String: el nombre del continente.
     * @param indiceContinente int: el índice que ocupa el continente seleccionado.
     * en los arrays de InfoAtlas, idéntico al índice del item seleccionado.
     */
    void mostrarContinente(String nombreContinente, int indiceContinente) {
        // Se pasa al constructor el índice que el continente ocupa en los arrays y el midlet:
        continente = new FormContinente(nombreContinente, indiceContinente, this);
        pantalla.setCurrent(continente);
    }

    /**
     * Método que muestra el formulario con los países elegidos.
     *
     * @param nombreContinente String: el nombre del continente.
     * @param indiceContinente int: el índice que ocupa el continente previamente
     * seleccionado en los arrays de InfoAtlas, idéntico al índice del item seleccionado.
     * @param paisesSeleccionados boolean[]: array de booleans que contiene true
     * en los índices de los países seleccionados y false en los no seleccionados.
     * El array es equivalente al array de países en InfoAtlas.
     */
    void mostrarPaises(String nombreContinente, int indiceContinente, boolean[] paisesSeleccionados) {
        paises = new FormPaises(nombreContinente, indiceContinente, paisesSeleccionados, this);
        pantalla.setCurrent(paises);
    }

    /**
     * Método que muestra una alerta si se pulsa el botón "Ver países"
     * sin tener ninguno seleccionado.
     */
    protected void mostrarAlertaNingunPaisSeleccionado() {
        Alert alerta = new Alert("Alerta", "No se ha seleccionado ningún país", null, AlertType.ALARM);
        // El usuario tendrá pulsar el botón de confirmación para salir de la alerta:
        alerta.setTimeout(Alert.FOREVER);
        pantalla.setCurrent(alerta, continente);
    }

    /**
     * Método que muestra un formulario con los créditos
     */
    void mostrarCreditos() {
        pantalla.setCurrent(formCreditos);
    }

}
