package midletatlas;

import java.io.IOException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.ImageItem;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.StringItem;

/**
 * Clase basada en un formulario (hereda de Form) que muestra el nombre de la aplicación,
 * el nombre del autor, su foto
 * (o caricatura) y la fecha de creación
 *
 * @author Jose Manuel Rozas Vilar
 */
public class FormCreditos extends Form implements CommandListener {

    private final MidletAtlas miMidlet;
    private final Command cmdSalir;

    /**
     *
     * @param titulo
     * @param miMidlet
     */
    public FormCreditos(String titulo, MidletAtlas miMidlet) {
        super(titulo);

        this.miMidlet = miMidlet;

        this.append(new StringItem("Midlet", "Conoce tus continentes", Item.PLAIN));

        try {
            Image foto = Image.createImage("/resources/foto.png");
            this.append(new ImageItem("Jose Manuel Rozas Vilar", foto, ImageItem.LAYOUT_CENTER, "asda", Item.PLAIN));
        } catch (IOException ex) {
            System.out.println("Error creando la imagen de la foto: " + ex.toString());
        }

        this.append(new StringItem("", "Creado el 9 de diciembre de 2013", Item.PLAIN));

        this.cmdSalir = new Command("Salir", Command.EXIT, 1);
        this.addCommand(cmdSalir);
        this.setCommandListener(this);
    }

    /**
     *
     * @param c
     * @param d
     */
    public void commandAction(Command c, Displayable d) {
        if (c == cmdSalir) {
            miMidlet.salirMidlet();
        }
    }

}
