package midletatlas;

import javax.microedition.lcdui.*;

/**
 * Clase basada en un formulario (hereda de Form) que contiene, para un continente dado
 * (pasado como parámetro al
 * constructor), un título con el nombre del continente, un mapa del continente y una
 * lista de selección múltiple
 * (ChoiceGroup) con algunos países del continente
 *
 * @author Jose Manuel Rozas Vilar
 */
public class FormContinente extends Form implements CommandListener {

    private final MidletAtlas miMidlet;

    // El índice del continente seleccionado,
    // vale lo mismo para los arrays de InfoAtlas:
    private final int indiceContinente;

    // El objeto donde se encuentra toda la información de continentes y países,
    // guardada en arrays (imágenes y textos):
    private final InfoAtlas infoAtlas;

    private final String nombreContinente;

    // La imagen de continente:
    private final Image ficheroImagen;

    // Tecto con una breve descripción del continente:
    private final StringItem descripcionContinente;

    // ChoiceGroup donde se muestran varios países del continente para seleccionar:
    private final ChoiceGroup paisesOpciones;

    // Cantidad de países de cada continente
    // que se van a mostrar para ser seleccionados, en pricipio 3:
    private final int cantidadPaises;

    private final Command cmdVolver, cmdVerPaises;

    /**
     * Constructor
     *
     * @param titulo String: el nombre del continente.
     * @param indiceContinente int: el índice que ocupa el continente en los arrays.
     * @param miMidlet MidletAtlas: referencia al midlet.
     */
    public FormContinente(String titulo, int indiceContinente, MidletAtlas miMidlet) {
        super(titulo);

        this.indiceContinente = indiceContinente;
        this.miMidlet = miMidlet;

        // Los datos del continente (nombre, descripción, imagen y países)
        // se obtienen a partir del índice del continente
        // seleccionado y a través de los arrays en infoAtlas:
        this.infoAtlas = new InfoAtlas();
        this.ficheroImagen = Image.createImage(infoAtlas.imagenContinentes[indiceContinente]);
        ImageItem imagenContinente = new ImageItem("", ficheroImagen, Item.LAYOUT_CENTER, "Africa", Item.PLAIN);
        // Añadir la imagen al formulario:
        this.append(imagenContinente);

        String textoDescripcionContinente = infoAtlas.descripcionContinentes[indiceContinente];
        descripcionContinente = new StringItem("", textoDescripcionContinente, Item.PLAIN);
        // Añadir la descripción al formulario:
        this.append(descripcionContinente);

        this.nombreContinente = infoAtlas.nombreContinentes[indiceContinente];
        paisesOpciones = new ChoiceGroup("Algunos países de " + nombreContinente, ChoiceGroup.MULTIPLE);
        cantidadPaises = infoAtlas.nombrePaises[indiceContinente].length;
        for (int i = 0; i < cantidadPaises; i++) { // 3 paises en principio.
            // Añadir los nombres de los países de éste continente al ChoiceGroup:
            paisesOpciones.append(infoAtlas.nombrePaises[indiceContinente][i], null);
        }
        // Añadir el ChoiceGroup con los países al formulario:
        this.append(paisesOpciones);

        // Crear Commands:
        cmdVolver = new Command("Volver", Command.BACK, 1);
        cmdVerPaises = new Command("Ver países", Command.OK, 1);
        // Añadir Commands:
        this.addCommand(cmdVolver);
        this.addCommand(cmdVerPaises);
        // Establecer Listener:
        this.setCommandListener(this);
    }

    //*********************************************************************************
    /**
     * @param c
     * @param d
     */
    public void commandAction(Command c, Displayable d) {
        if (c == cmdVolver) {
            // Volver por donde vino:
            miMidlet.mostrarListaContinentes();
        } else if (c == cmdVerPaises) {
            // Variable para saber si hay algún país seleccionado.
            // Al principio no hay ninguno, false:
            boolean algunPaisSeleccionado_flag = false;

            // Array de booleans del mismo tamaño que países.
            boolean[] paisesSeleccionados = new boolean[cantidadPaises];

            // Se guardará true en los índices de un países seleccionados
            // y false en los que no mediante el método getSelectedFlags():
            paisesOpciones.getSelectedFlags(paisesSeleccionados);

            // Recorremos el array de paises seleccionados para comprobar
            // que se ha seleccionado algún país;
            for (int i = 0; i < paisesSeleccionados.length; i++) {
                // Comprobar que hay algún país seleccionado:
                if (paisesSeleccionados[i]) {
                    // Con que haya algún país seleccionado ya podemos hacer
                    // la variable flag igual a true, y salir del bucle:
                    algunPaisSeleccionado_flag = true;
                    break;
                }
            }
            // Si se ha seleccionado al menos un país, se muestra el formulario:
            if (algunPaisSeleccionado_flag) {
                miMidlet.mostrarPaises(nombreContinente, indiceContinente, paisesSeleccionados);
            } else {
                // Si no se ha seleccionado ningún país se muestra una alerta:
                miMidlet.mostrarAlertaNingunPaisSeleccionado();
            }
        }
    }

}
