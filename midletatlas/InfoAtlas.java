package midletatlas;

import java.io.IOException;
import javax.microedition.lcdui.Image;

/**
 * Clase que contiene toda la información sobre continentes y países: nombres,
 * descripciones, mapas, etc
 * Variables y métodos estáticos: no será necesario crear un objeto para llamarlos.
 *
 * @author Jose Manuel Rozas Vilar
 */
public class InfoAtlas {

    /**
     * Los nombres de los 5 continentes.
     * El orden de los índices siempre será el mismo en el resto de arrays de continentes.
     */
    protected final String[] nombreContinentes = {"África", "América", "Asia", "Europa", "Oceanía"};

    /**
     * En donde se guardarán las 5 imágenes de cada continente.
     */
    protected final Image[] imagenContinentes = new Image[5];

    /**
     * Descripción de cada uno de los 5 continentes,
     * respetando siempre el mismo orden en los índices.
     */
    protected final String[] descripcionContinentes = {
        "El tercer continente más extenso. Población de mil millones de habitantes. Contiene 54 países.",
        "El segundo continente más extenso. Concentra cerca del 12% de la población humana.",
        "Es el continente más extenso y poblado de la Tierra, con 4.200.000.000 de habitantes, el 60% de la población mundial.",
        "Es el continente más densamente poblado. Europa es la cuna de la cultura occidental.",
        "Oceanía es un continente insular de la Tierra constituido por la plataforma continental de Australia e islas adyacentes."
    };

    /**
     * Los nombres de los países.
     * El primer índice de este array bidimensional corresponde a los 5 continentes.
     */
    protected final String[][] nombrePaises = {
        {"Angola", "Niger", "Zambia"},
        {"Belice", "Paraguay", "Surinam"},
        {"Mongolia", "Sri Lanka", "Uzbekistán"},
        {"Austria", "Irlanda", "Noruega"},
        {"Australia", "Nueva Zelanda", "Papua Nueva Guinea"}
    };

    /**
     * Las imágenes de los países.
     * El primer índice de este array bidimensional corresponde a los 5 continentes.
     */
    protected final Image[][] imagenPaises = new Image[5][3];

    //*******************************************************************************
    /**
     * Constructor
     */
    public InfoAtlas() {
        // Crear las imágenes de los continentes:
        try {
            imagenContinentes[0] = Image.createImage("/resources/continentes/africa.png");
            imagenContinentes[1] = Image.createImage("/resources/continentes/america.png");
            imagenContinentes[2] = Image.createImage("/resources/continentes/asia.png");
            imagenContinentes[3] = Image.createImage("/resources/continentes/europa.png");
            imagenContinentes[4] = Image.createImage("/resources/continentes/oceania.png");
        } catch (IOException ex) {
            System.err.println("Error creando las imágenes de los continentes: ");
            System.err.println(ex.toString());
        }

        // Crear las imágenes de los países:
        try {
            imagenPaises[0][0] = Image.createImage("/resources/paises/angola.png");
            imagenPaises[0][1] = Image.createImage("/resources/paises/niger.png");
            imagenPaises[0][2] = Image.createImage("/resources/paises/zambia.png");

            imagenPaises[1][0] = Image.createImage("/resources/paises/belice.png");
            imagenPaises[1][1] = Image.createImage("/resources/paises/paraguay.jpg");
            imagenPaises[1][2] = Image.createImage("/resources/paises/surinam.png");

            imagenPaises[2][0] = Image.createImage("/resources/paises/mongolia.png");
            imagenPaises[2][1] = Image.createImage("/resources/paises/srilanka.png");
            imagenPaises[2][2] = Image.createImage("/resources/paises/uzbekistan.png");

            imagenPaises[3][0] = Image.createImage("/resources/paises/austria.png");
            imagenPaises[3][1] = Image.createImage("/resources/paises/irlanda.png");
            imagenPaises[3][2] = Image.createImage("/resources/paises/noruega.png");

            imagenPaises[4][0] = Image.createImage("/resources/paises/australia.png");
            imagenPaises[4][1] = Image.createImage("/resources/paises/nuevazelanda.png");
            imagenPaises[4][2] = Image.createImage("/resources/paises/papuanuevaguinea.png");

        } catch (IOException ex) {
            System.err.println("Error creando las imágenes de los paises: ");
            System.err.println(ex.toString());
        }
    }
}
