package midletatlas;

import javax.microedition.lcdui.*;

/**
 * Clase basada en un formulario (hereda de Form) que contiene los mapas de los países
 * seleccionados en la lista de
 * selección múltiple (ChoiceGroup) de FormContinente
 *
 * @author Jose Manuel Rozas Vilar
 */
public class FormPaises extends Form implements CommandListener {

    private final int indiceContinente;
    private final boolean[] paisesSeleccionados;
    private final MidletAtlas miMidlet;

    private final InfoAtlas infoAtlas;
    private final String nombreContinente;

    private Image imagenPais;
    private ImageItem imageItem;

    private final Command cmdVolver;

    //******************************************************************
    /**
     * Constructor
     *
     * @param nombreContinente
     * @param indiceContinente
     * @param paisesSeleccionados
     * @param miMidlet
     */
    public FormPaises(String nombreContinente, int indiceContinente, boolean[] paisesSeleccionados, MidletAtlas miMidlet) {
        super(nombreContinente);

        this.indiceContinente = indiceContinente;
        this.paisesSeleccionados = paisesSeleccionados;
        this.miMidlet = miMidlet;

        this.infoAtlas = new InfoAtlas();
        this.nombreContinente = infoAtlas.nombreContinentes[indiceContinente];

        for (int i = 0; i < paisesSeleccionados.length; i++) {
            if (paisesSeleccionados[i]) {
                imagenPais = Image.createImage(infoAtlas.imagenPaises[indiceContinente][i]);
                imageItem = new ImageItem(infoAtlas.nombrePaises[indiceContinente][i], imagenPais, Item.LAYOUT_CENTER, "", Item.PLAIN);
                this.append(imageItem);
            }
        }

        cmdVolver = new Command("Volver", Command.BACK, 1);
        this.addCommand(cmdVolver);
        this.setCommandListener(this);
    }

    //*****************************************************************************
    /**
     *
     * @param c
     * @param d
     */
    public void commandAction(Command c, Displayable d) {
        if (c == cmdVolver) {
            miMidlet.mostrarContinente(nombreContinente, indiceContinente);
        }
    }

}
