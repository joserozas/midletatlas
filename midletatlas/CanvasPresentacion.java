package midletatlas;

import java.io.IOException;
import javax.microedition.lcdui.*;

/**
 * Clase basada en Canvas (hereda de Canvas) que aparece al iniciar la aplicación. Sirve
 * como presentación del midlet y
 * una vez que se pulse el comando "Comenzar" se pasará a la lista de continentes y ya no
 * volverá a aparecer
 *
 * @author Jose Manuel Rozas Vilar
 */
public class CanvasPresentacion extends Canvas implements CommandListener {

    private final MidletAtlas miMidlet;

    // Imagen con el logotipo:
    private Image imagenPresentacion;

    // Los botones:
    private final Command cmdSalir, cmdContinuar;

    //**********************************************************************
    /**
     * Constructor
     *
     * @param miMidlet MidletAtlas: el midlet de la app
     */
    public CanvasPresentacion(MidletAtlas miMidlet) {
        this.miMidlet = miMidlet;

        // Imagen para la presentación:
        String rutaImagen = "/resources/logo_128x128.png";
        try {
            imagenPresentacion = Image.createImage(rutaImagen);
        } catch (IOException e) {
            System.err.println("Error cargando la imagen " + rutaImagen);
            System.err.println(e.toString());
        }

        // Commands:
        cmdSalir = new Command("Salir", Command.EXIT, 0);
        cmdContinuar = new Command("Continuar", Command.OK, 1);

        // Añadir Commands al canvas:
        this.addCommand(cmdSalir);
        this.addCommand(cmdContinuar);

        // Establecer Listener:
        this.setCommandListener(this);

    }

    //**************************************************************************
    /**
     * El método paint() de la clase Canvas
     *
     * @param g
     */
    protected void paint(Graphics g) {
        // Centro de la pantalla:
        int centroX = this.getWidth() / 2;
        int centroY = this.getHeight() / 2;

        // Fondo:
        g.setColor(250, 127, 0); //  naranja.
        g.fillRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);

        //_________________________
        // Dibujar imagen inmutable
        // Imagen:
        g.drawImage(imagenPresentacion, centroX, centroY / 2, Graphics.HCENTER | Graphics.VCENTER);

        // Texto del título, en grande:
        g.setColor(0x000000);
        g.setFont(Font.getFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_LARGE));
        g.drawString("Conoce tus continentes", centroX, centroY, Graphics.HCENTER | Graphics.TOP);
        // Texto adicional, tamaño medio
        g.setFont(Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_MEDIUM));
        g.drawString("Tarea 2 para PMDM", centroX, centroY + 30, Graphics.HCENTER | Graphics.TOP);

    }

    //***********************************************************************
    /**
     *
     * @param c
     * @param d
     */
    public void commandAction(Command c, Displayable d) {
        if (c == cmdSalir) {
            miMidlet.mostrarCreditos();
        } else if (c == cmdContinuar) {
            miMidlet.mostrarListaContinentes();
        }
    }

}
