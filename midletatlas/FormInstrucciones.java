package midletatlas;

import javax.microedition.lcdui.*;

/**
 * Clase basada en un formulario (hereda de Form) que muestra el nombre de la aplicación y
 * unas breves instrucciones de
 * uso
 *
 * @author Jose Manuel Rozas Vilar
 */
public class FormInstrucciones extends Form implements CommandListener {

    private final MidletAtlas miMidlet;

    // StringItem con el texto de las instrucciones:
    private final StringItem strItemInstrucciones;

    private final Command cmdVolver, cmdSalir;

    /**
     *
     * @param titulo
     * @param miMidlet
     */
    public FormInstrucciones(String titulo, MidletAtlas miMidlet) {
        super(titulo);

        // Referencia al midlet:
        this.miMidlet = miMidlet;

        // StringItem, texto con las instrucciones:
        String textoInstrucciones
                = "En la primera pantalla se muestra una lista con los 5 continentes."
                + "\nSelecciona uno de ellos por medio de las teclas de dirección "
                + "y acepta la selección por medio del botón central.\n"
                + "\nDentro de cada continente, puedes seleccionar uno o varios de los países "
                + "marcando con el botón central y aceptando la selección con el botón \"ver paises\""
                + "\nEntonces podrás ver el mapa de cada uno de los países que hayas seleccionado.";
        strItemInstrucciones = new StringItem("", textoInstrucciones, Item.PLAIN);

        // Command:
        cmdSalir = new Command("Salir", Command.EXIT, 0);
        cmdVolver = new Command("Volver", Command.OK, 1);

        // Añadir Item al formulario:
        this.append(strItemInstrucciones);

        // añadir Command al Form:
        this.addCommand(cmdSalir);
        this.addCommand(cmdVolver);

        // Establecer Listener:
        this.setCommandListener(this);
    }

    /**
     *
     * @param c
     * @param d
     */
    public void commandAction(Command c, Displayable d) {
        if (c == cmdVolver) {
            miMidlet.mostrarListaContinentes();
        } else if (c == cmdSalir) { // antes de salir mostramos los créditos.
            miMidlet.mostrarCreditos();
        }
    }
}
